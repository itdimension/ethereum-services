pragma solidity ^0.4.8;

import "./BaseToken.sol";
import "./Destructible.sol";

contract RMToken is BaseToken, Destructible {

    string public name = "RM Token";
    uint8 public decimals = 4;
    string public symbol = "RM";
    string public version = '0.1';
    
    address[] public investors;
    
    function RMToken (
        uint256 _initialAmount
        ) {
        balances[msg.sender] = _initialAmount;
        totalSupply = _initialAmount;
    }
}
