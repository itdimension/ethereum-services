1. Install all dependencies with `npm install` 
2. Create ethereum account for token-service administrator with `node create_account.js`
3. Change settings in `config.json` file if needed 
4. Start the token-service with `node .` or `node token-service.js`, wait until it will be initialized
5. You can test with `node tests/token.js`, it will start test-server on 9080 port by default
   ```
   $ curl -X GET http://localhost:9080/testtransfer
   $ curl -X GET http://localhost:9080/testbalance
   ```
6. You can use `forever` utility for best experience
7. Install `forever` with `npm isntall -g forever`
8. Create forever config with `./create-forever-config.sh`
9. Start the token-service with `forever start token-service-forever.json`, you can find all logs in subdir `forever/log`
10. More about forever https://github.com/foreverjs/forever
