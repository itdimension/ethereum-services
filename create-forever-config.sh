#!/bin/bash

log() {
  tput setaf 1
  echo $1
  tput sgr0
}

dir=`pwd`

cp ${dir}/forever/token-service-forever.json.tmpl ${dir}/token-service-forever.json
sed -i "s@{SOURCE_DIR}@${dir}@g" token-service-forever.json

log "CREATED: ${dir}/token-service-forever.json"
